﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    #region Declare Variables
    [SerializeField] private TowerFactory towerFactory;

    [HideInInspector] public Waypoint exploredFrom;
    [HideInInspector] public bool isExplored = false;
    public bool isPlaceable = true;
    public static int GridSize { get; } = 10;
    private Vector2Int gridPos;
    #endregion

    public Vector2Int GetGridPos()
    {
        var position = transform.position;
        return new Vector2Int(Mathf.RoundToInt(position.x / GridSize) ,
                              Mathf.RoundToInt(position.z / GridSize));
    }

    void OnMouseOver()
    {

        if (Input.GetMouseButtonDown(0))
        {
            if (isPlaceable)
            {
                towerFactory.AddTower(this);
            } else
            {
                Debug.Log($"{gameObject.name} is NOT Placeable.");
            }

        }
    }


}
