﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class PathFinder : MonoBehaviour
{
    #region Declare Variable
    [SerializeField] private Waypoint startWaypont, endWaypoint;
    private List<Waypoint> path=new List<Waypoint>();//tod make it private
    private Dictionary<Vector2Int,Waypoint> grid=new Dictionary<Vector2Int, Waypoint>();
    private Queue<Waypoint> queue =new Queue<Waypoint>();
    private bool isRunning=true;
    private Waypoint searchCenter;
    private readonly Vector2Int[] directions =
    {
        Vector2Int.up,
        Vector2Int.down,
        Vector2Int.right,
        Vector2Int.left
    };
    #endregion

    private void Start()
    {
        CalculatePath();
    }

    private void CalculatePath()
    {
        LoadBlocks();
        BreadthFirstSearch();
        CreatePath();
    }

    public List<Waypoint> GetPath()
    {
        return path;
    }

    private void CreatePath()
    {
        SetAtPath(endWaypoint);

        var previous = endWaypoint.exploredFrom;
        while (previous!=startWaypont)
        {
            SetAtPath(previous);
            previous = previous.exploredFrom;
        }
        SetAtPath(startWaypont);
        path.Reverse();
    }

    private void SetAtPath(Waypoint waypoint)
    {
        path.Add(waypoint);
        waypoint.isPlaceable = false;
    }

    private void BreadthFirstSearch()
    {
        queue.Enqueue(startWaypont);
        while (queue.Count>0 && isRunning)
        {
            searchCenter=queue.Dequeue();
            HaltIfEndFound();
            ExploreNeighbours();
            searchCenter.isExplored = true;
        }
    }

    private void HaltIfEndFound()
    {
        if (searchCenter == endWaypoint)
        {
            isRunning = false;
        }

    }

    private void ExploreNeighbours()
    {
        if(!isRunning)    return;

        for (int i = 0; i < directions.Length; i++)
        {
            Vector2Int neighbourCoordinate = searchCenter.GetGridPos() + directions[i];
            if (grid.ContainsKey(neighbourCoordinate))
            {
                QueueNewNeighbours(neighbourCoordinate);
            }
        }
    }

    private void QueueNewNeighbours(Vector2Int neighbourCoordinate)
    {
        Waypoint neighbour = grid[neighbourCoordinate];

        if (neighbour.isExplored || queue.Contains(neighbour)) return;

        queue.Enqueue(neighbour);
        neighbour.exploredFrom = searchCenter;


    }

    private void LoadBlocks()
    {
        var waypoints = FindObjectsOfType<Waypoint>();
        for (int i = 0; i < waypoints.Length; i++)
        {
            var gridPos = waypoints[i].GetGridPos();
            if(grid.ContainsKey(gridPos))
                Debug.LogWarning($"Skipping overlapping on block {waypoints[i].name}");
            else
            {
                grid.Add(gridPos,waypoints[i]);
            }
        }
    }
}
