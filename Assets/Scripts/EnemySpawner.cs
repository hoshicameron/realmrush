﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    [SerializeField] private EnemyMovement enemyPrefab;
    [Range(1.0f,120.0f)]
    [SerializeField] private float secondsBetweenSpawns=5.0f;
    [SerializeField] private TextMeshProUGUI spawnedEnemies;
    [SerializeField] private AudioClip spawnClip;
    private AudioSource audioSource;

    private int score=0;
    // Start is called before the first frame update
    void Start()
    {
        spawnedEnemies.text = score.ToString();
        AddAudioComponent();
        StartCoroutine(SpawnEnemy());
    }

    private void AddAudioComponent()
    {
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.playOnAwake = false;
    }

    IEnumerator SpawnEnemy()
    {
        while (true)
        {
            AddScore();
            InstantiateEnemy();
            yield return new WaitForSeconds(secondsBetweenSpawns);
        }
    }

    private void InstantiateEnemy()
    {
        Instantiate(enemyPrefab, transform.position,
            Quaternion.identity).transform.parent = transform;
        //Play Spawn Audio
        audioSource.clip = spawnClip;
        audioSource.Play();
    }

    private void AddScore()
    {
        score++;
        spawnedEnemies.text = score.ToString();
    }
}
