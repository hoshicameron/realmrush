﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private int playerHealth=5;
    [SerializeField] private int healthDecrease=1;
    [SerializeField] private TextMeshProUGUI healthText;
    [SerializeField] private AudioClip damageAudioclip;
    private AudioSource audioSource;
    private int enemyLayer;

    private void Start()
    {
        enemyLayer = LayerMask.NameToLayer("Enemy");
        healthText.text = Health.ToString();

        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.playOnAwake = false;

    }

    public int Health
    {
        get { return playerHealth; }
        set { playerHealth = value; }
    }

    public int HealthDecrease
    {
        get { return healthDecrease; }
        set { healthDecrease = value; }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer!=enemyLayer)    return;

        Health-=HealthDecrease;
        healthText.text = Health.ToString();
        audioSource.clip = damageAudioclip;
        audioSource.Play();

    }

    private void Update()
    {
        if (Health==0)
        {
            print("Player health is over, Restarting the game.");
        }
    }
}
