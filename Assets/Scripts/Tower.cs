﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [Range(1.0f,100.0f)]
    [SerializeField] private float attackRange = 30.0f;
    private Transform objectToPan;
    private Transform targetEnemy;
    [SerializeField] private ParticleSystem gun;
    private Waypoint baseWaypoint;


    public Waypoint BaseWaypoint
    {
        get { return baseWaypoint; }
        set { baseWaypoint = value; }
    }

    private void Start()
    {
        objectToPan = transform.GetChild(0);

    }

    // Update is called once per frame
    void Update()
    {
        SetTargetEnemy();
        if (targetEnemy)
        {
            objectToPan.LookAt(targetEnemy);
            FireAtEnemy();
        } else
        {
            Fire(false);
        }
    }

    private void SetTargetEnemy()
    {
        var sceneEnemy = FindObjectsOfType<EnemyDamage>();
        if(sceneEnemy.Length==0)    return;
        Transform closestEnemy = sceneEnemy[0].transform;
        foreach (var enemy in sceneEnemy)
        {
            closestEnemy=GetClosest(closestEnemy, enemy.transform);
        }

        targetEnemy = closestEnemy;
    }

    private Transform GetClosest(Transform transformA, Transform transformB)
    {
        float distanceA = Vector3.Distance(transform.position,transformA.position);
        float distanceB = Vector3.Distance(transform.position, transformB.position);

        if (distanceA<distanceB)
            return transformA;

        return transformB;
    }

    private void FireAtEnemy()
    {
        float distanceToEnemy = Vector3.Distance(targetEnemy.transform.position, this.transform.position);
        if (distanceToEnemy < attackRange)
        {
            Fire(true);
        }else
            Fire(false);
    }

    private void Fire(bool isActive)
    {
        var em = gun.emission;
        em.enabled = isActive;
    }
}
