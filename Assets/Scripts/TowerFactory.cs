﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour
{
    [SerializeField] private Tower towerPrefab;
    [SerializeField] private int towerLimit=5;
    private int towerNum;
    private Queue<Tower> towerQueue=new Queue<Tower>();
    public void AddTower(Waypoint waypoint)
    {
        towerNum = towerQueue.Count;
        if (towerNum < towerLimit)
        {
            InstantiateTower(waypoint);
        } else
        {
            MovingExistingTower(waypoint);
        }
    }
    private void InstantiateTower(Waypoint waypoint)
    {
        Tower tower =
            Instantiate(towerPrefab, waypoint.transform.position, Quaternion.identity);
        tower.transform.parent = transform;
        waypoint.isPlaceable = false;

        tower.BaseWaypoint = waypoint;

        towerQueue.Enqueue(tower);
    }
    private void MovingExistingTower(Waypoint waypoint)
    {
        Tower tower = towerQueue.Dequeue();
        //Set the base waypoint to true,so player can add tower to that in future
        tower.BaseWaypoint.isPlaceable=true;
        //Change the position of the tower
        tower.transform.position = waypoint.transform.position;
        //Set the tower base to new waypoint
        tower.BaseWaypoint = waypoint;

        waypoint.isPlaceable = false;

        towerQueue.Enqueue(tower);
    }
}
