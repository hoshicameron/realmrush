﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{
    [SerializeField] private ParticleSystem explosionParticle;
    [SerializeField] private ParticleSystem suicideParticle;
    [SerializeField] private ParticleSystem hitParticle;
    [SerializeField] private int life = 5;
    [SerializeField] private AudioClip explosionAudioClip;
    [SerializeField] private AudioClip hitAudioClip;
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.playOnAwake = false;

    }

    private void OnParticleCollision(GameObject other)
    {
        life--;
        HitSFX();
        if (life<1)
        {
            DeathSequence();
        }
    }

    private void DeathSequence()
    {
        Instantiate(explosionParticle, transform.position, Quaternion.identity);
        AudioSource.PlayClipAtPoint(explosionAudioClip,Camera.main.transform.position ,1f);
        Destroy(gameObject);
    }

    private void HitSFX()
    {
        hitParticle.Play();
        audioSource.clip = hitAudioClip;
        audioSource.Play();
    }

    public void PlaySuicideParticle()
    {
        Instantiate(suicideParticle, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
