﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField] private float blockVisitTime = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        PathFinder pathFinder = FindObjectOfType<PathFinder>();
        var path = pathFinder.GetPath();
        StartCoroutine(FollowPath(path));
    }
    IEnumerator FollowPath(List<Waypoint> path)
    {
        for (int i = 0; i < path.Count; i++)
            {
                transform.position = path[i].transform.position;
                yield return new WaitForSeconds(blockVisitTime);
            }
        gameObject.SendMessage("PlaySuicideParticle");
    }
}
