﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
[SelectionBase]
[RequireComponent(typeof(Waypoint))]
public class CubeEditor : MonoBehaviour
{
    #region Declare Variables
    [SerializeField]private TextMeshPro textMeshPro;
    private Waypoint waypoint;

    #endregion

    private void Awake()
    {
        waypoint = GetComponent<Waypoint>();
    }

    void Update()
    {
        SnapToGrid();
        UpdateLabel();
    }

    private void SnapToGrid()
    {
        int gridSize = Waypoint.GridSize;
        transform.position = new Vector3(waypoint.GetGridPos().x*gridSize,
                                      0,
                                      waypoint.GetGridPos().y*gridSize);
    }

    private void UpdateLabel()
    {
        int gridSize = Waypoint.GridSize;
        string labelText = $"{waypoint.GetGridPos().x},{waypoint.GetGridPos().y }";
        textMeshPro.text = labelText;
        gameObject.name = $"Cube {labelText}";
    }


}
